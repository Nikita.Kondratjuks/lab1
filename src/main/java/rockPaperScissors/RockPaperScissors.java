package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();

    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    // currentRound is made so that we can keep a while loop going until the game starts or the player decides to stop the game
    boolean currentRound = true;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors
        // while loop so that the player can keep the count on the amount of round and score that the human and computer has
        while (true) {
            if (currentRound) {
                System.out.println("Let's play round " + roundCounter);
                currentRound = false;
            }
            String userInput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (!rpsChoices.contains(userInput)) {
                System.out.println("I do not understand "+ userInput +". Could you try again?");
            }
            String aiInput = aiInput();

            rpsAlgorithm(userInput,aiInput);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            // if the player says anything else then "n" then the game continous or the loop breaks and the game ends.
            String keepPlaying = readInput("Do you wish to continue playing? (y/n)?");
            if (keepPlaying.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            } else {
                roundCounter++;
                currentRound = true;
            }
        }
    }
    /*This is the rps algorithm that checks for if/ else if / else statments to check what is correct.
    Wanted to try switch command with different types of cases, but couldn't figure it out. so I settled for the good old  if statments.
     */
    public void rpsAlgorithm(String userInput, String aiInput){
        if (userInput.equals(aiInput)) {
            System.out.println("Human chose " + userInput + " computer chose " + aiInput + ". It's a tie!");
        } else if (userInput.equals("rock")){
            if (aiInput.equals("paper")) {
                System.out.println("Human chose " + userInput + " computer chose " + aiInput + ". Computer wins!");
                computerScore++;
            } else {
                System.out.println("Human chose " + userInput + " computer chose " + aiInput + ". Human wins!");
                humanScore++;
            }
        } else if (userInput.equals("paper")) {
            if (aiInput.equals("scissors")) {
                System.out.println("Human chose " + userInput + " computer chose " + aiInput + ". Computer wins!");
                computerScore++;
            } else {
                System.out.println("Human chose " + userInput + " computer chose " + aiInput + ". Human wins!");
            }
        } else if (userInput.equals("scissors")) {
            if (aiInput.equals("rock")) {
                System.out.println("Human chose " + userInput + " computer chose " + aiInput + ". Computer wins!");
                computerScore++;
            } else {
                System.out.println("Human chose " + userInput + " computer chose " + aiInput + ". Human wins!");
                humanScore++;
            }
        }

    }
    //ai random choice inputs.
    public String aiInput() {
        int randomChoice =(int) (Math.random() * rpsChoices.size());
        return rpsChoices.get(randomChoice);

    }

    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
